import BaseHTTPServer
import CGIHTTPServer
import json
import os
import threading
import tkFileDialog
import tkMessageBox
import ttk
import urllib
from Tkinter import *


class Handler(CGIHTTPServer.CGIHTTPRequestHandler):
    cgi_directories = ["/cgi-bin/"] #CGI Files & Folders Go Here

    def is_executable(self, path):
        """Test whether argument path is an executable file."""
        return True

    def is_python(self, path):
        """Test whether argument path is a Python script."""
        return True

    def url_collapse_path(self, path):
        """
        Given a URL path, remove extra '/'s and '.' path elements and collapse
        any '..' references and returns a colllapsed path.

        Implements something akin to RFC-2396 5.2 step 6 to parse relative paths.
        The utility of this function is limited to is_cgi method and helps
        preventing some security attacks.

        Returns: The reconstituted URL, which will always start with a '/'.

        Raises: IndexError if too many '..' occur within the path.

        """
        # Query component should not be involved.
        path, _, query = path.partition('?')
        path = urllib.unquote(path)

        # Similar to os.path.split(os.path.normpath(path)) but specific to URL
        # path semantics rather than local operating system semantics.
        path_parts = path.split('/')
        head_parts = []
        for part in path_parts[:-1]:
            if part == '..':
                head_parts.pop()  # IndexError if more '..' than prior parts
            elif part and part != '.':
                head_parts.append(part)
        if path_parts:
            tail_part = path_parts.pop()
            if tail_part:
                if tail_part == '..':
                    head_parts.pop()
                    tail_part = ''
                elif tail_part == '.':
                    tail_part = ''
        else:
            tail_part = ''

        if query:
            tail_part = '?'.join((tail_part, query))

        splitpath = ('/' + '/'.join(head_parts), tail_part)
        collapsed_path = "/".join(splitpath)

        return collapsed_path

    def is_cgi(self):
        """Test whether self.path corresponds to a CGI script.

        Returns True and updates the cgi_info attribute to the tuple
        (dir, rest) if self.path requires running a CGI script.
        Returns False otherwise.

        If any exception is raised, the caller should assume that
        self.path was rejected as invalid and act accordingly.

        The default implementation tests whether the normalized url
        path begins with one of the strings in self.cgi_directories
        (and the next character is a '/' or the end of the string).
        """
        collapsed_path = self.url_collapse_path(self.path)
        dir_sep = collapsed_path.find('/', 1)
        head, tail = collapsed_path[:dir_sep], collapsed_path[dir_sep + 1:]
        print "SPV head: " + head
        print "SPV tail: " + tail
        print "SPV self.cgi_directories: " + str(self.cgi_directories)
        print "SPV path: " + self.path
        for path in self.cgi_directories:
            if path in self.path and self.path is not "/":
                print "SPV CGI: " + self.path
                self.cgi_info = head, tail
                return True
        if head in self.cgi_directories:
            self.cgi_info = head, tail
            return True
        return False


PORT = 80
httpd = BaseHTTPServer.HTTPServer(("", PORT), Handler)


# Server start function and serve forever on t1 thread
def startserver():
    t1 = threading.Thread(target=httpd.serve_forever)
    t1.start()
    print "serving at port", PORT


# Send shutdown signal to http server
def stopserver():
    httpd.shutdown()
    print "server at port", PORT, "has been stopped"


d = []  # DICTIONARY


# Tkinter GUI Class
class ServerGUI:
    def __init__(self, master):

        self.master = master
        self.datawin = Toplevel(master)
        self.datawin.title('Display Data')
        self.datawin.state('withdrawn')
        master.title("ZIGOR Server")
        master.geometry('350x250')
        master.resizable(False, False)
        # Window Tabs
        notebook = ttk.Notebook(master)
        params = ttk.Frame(notebook)
        server = ttk.Frame(notebook)
        notebook.add(server, text='Server Options')
        notebook.add(params, text='View Status')
        self.dataframe = ttk.Panedwindow(self.datawin, orient=HORIZONTAL)
        self.frame2 = ttk.Frame(self.dataframe, relief=SUNKEN)
        self.frame1 = ttk.Frame(self.dataframe, relief=SUNKEN)
        # File reader to edit cgi files
        readerframe = ttk.Frame(params, height=15, width=45)
        self.path = ttk.Entry(params, width=35)
        self.reader = Text(readerframe, width=40, height=10)
        notebook.pack()
        self.path.pack(anchor="e", pady=5, padx=5)
        Label(params, text="File Name").place(x=15, y=5)
        readerframe.pack()
        self.reader.pack()
        ttk.Button(readerframe, text="Save Data", command=self.savedata).pack(side=RIGHT)
        ttk.Button(readerframe, text="Read Data", command=self.getdata).pack(side=LEFT)
        ttk.Button(readerframe, text="Print Data", command=self.printarray).pack()  # TEMP TEST
        ttk.Button(self.datawin, text="Generate JSON", command=self.updatejson).pack()  # TEMP TEST
        # Server Control
        Label(server, text="Servidor para paginas de dispositivos", justify=CENTER, padx=10, pady=10).pack()
        ttk.Button(server, text="Start Server", command=startserver).pack()
        ttk.Button(server, text="Stop Server", command=stopserver).pack(side=BOTTOM)

    # Retrieve data from selected file
    def getdata(self):
        filename = str(os.path.basename(tkFileDialog.askopenfilename()))
        if True:
            self.reader.delete(1.0, END)
        while filename is '':  # Error handling for empty path
            answer = tkMessageBox.askretrycancel("Error", "No file selected")
            if answer is False:
                break
            else:
                filename = str(os.path.basename(tkFileDialog.askopenfilename()))
        self.path.delete(0, END)  # Clear the path area and
        self.path.insert(0, filename)  # display filename
        for line in open(filename, 'r'):
            if "aaData" in line:
                print line  # TEMPORAL
                json_start = line.find("{")
                json_end = line.find("}")
                data = line[json_start:json_end + 1].replace("'", '"')
                data = data.strip("['").strip("n']")
                print data  # TEMPORAL
                json_data = json.loads(data)
                i = 0
                for x in json_data['aaData']:
                    for y in x:
                        self.reader.insert(END, y + "\n")  # update reader with string in selected file
                        global d
                        d.append(y.strip("u'").strip("'"))
                        print d[i]  # TEMPORAL
                        i = i + 1
                        print i  # TEMPORAL
                        print d  # TEMPORAL
                self.reader.insert(END, "---------------\n")  # TEMPORAL Divider
            else:
                continue

    def printarray(self):
        print self.datawin.state()  # TEMPORAL
        self.datawin.state('normal')
        self.dataframe.pack(fill=BOTH, expand=True)
        self.dataframe.add(self.frame1, weight=1)
        self.dataframe.add(self.frame2, weight=1)
        global d
        datatree1 = ttk.Treeview(self.frame1)
        datatree1.pack(fill=BOTH, expand=True)
        datatree2 = ttk.Treeview(self.frame2)
        datatree2.pack(fill=BOTH, expand=True)
        datatree1.config(selectmode='none')
        datatree2.config(selectmode='none')
        i = 0
        for x in d:
            print i  # TEMPORAL
            if i % 2 == 0:
                datatree1.insert('', 'end', i, text=x)
            else:
                datatree2.insert('', 'end', i, text=x)
            i = i + 1
        self.datawin.resizable(False, False)

    # FILE EDITOR BUFFER ------------------------ TEST THIS DOESNT WORK WELL
    def updatejson(self):
        # global d
        filename = str(self.path.get())
        i = 0
        for x in d:
            print i  # TEMPORAL
            if i % 2 == 0:
                param = []
                param.append(x)
                buffer_ = x + ' Parameter'
                with open(filename, 'r') as infile:
                    data = infile.read().replace(x, buffer_)
                    with open(filename, 'w') as outfile:
                        outfile.write(data)
            else:
                value = []
                value.append(x)
                buffer_ = x + ' Value'
                with open(filename, 'r') as infile:
                    data = infile.read().replace(x, buffer_)
                    with open(filename, 'w') as outfile:
                        outfile.write(data)
            i = i + 1

            # TESTING

            # try:
            #     with open(filename, 'r') as infile:
            #         buffer_ = x + ' Test'           # TEST, REMOVE
            #         if x != buffer_:                # TEST, REMOVE
            #             data = infile.read().replace(x, buffer_)
            #             with open(filename, 'w') as outfile:
            #                 outfile.write(data)
            #         else:
            #             print 'this is garbage'     # TEST, REMOVE
            # except OSError as exception:
            #     print('ERROR: could not read file:')
            #     print('  %s' % exception)

            # TESTING

            # else:
            #     with open(filename, 'w') as outfile:
            #         outfile.write(data)
    # FILE EDITOR BUFFER ------------------------ TEST THIS DOESNT WORK WELL

    # SAVE DATA FUNCTION ------------------------ TEST THIS DOESNT WORK WELL
    # Save data from textbox to the selected file
    def savedata(self):
        if str(self.path.get()) == '':
            tkMessageBox.showerror("Error", "No file selected")  # Error handling for empty path
        else:
            data = str(self.reader.get("1.0", END))
            print data.splitlines()  # TEMPORAL
            answer = tkMessageBox.askyesno("Question",
                                           "Do you want to save this file?")  # Dialog box before replacing the file
            if answer is True:
                with open(str(self.path.get()), 'w') as myfile:  # Pure Garbage
                    myfile.write("""import cgi, cgitb
                                        cgitb.enable()
                                        form = cgi.FieldStorage()
                                        e = form.getvalue('ielem')
                                        
                                        if e == '0':
                                          print 'Content-Type: application/json'
                                          print""")
                    for line in data.splitlines():  # Pure Garbage
                        myfile.write(line + '\n')
    # SAVE DATA FUNCTION ------------------------ TEST THIS DOESNT WORK WELL

def main():
    window = Tk()
    serverGUI = ServerGUI(window)
    window.mainloop()


if __name__ == "__main__": main()
