from distutils.core import setup

setup(
    name='ServerUI',
    version='0.1',
    packages=[''],
    url='',
    license='',
    author='facevedo',
    author_email='facevedo@zigor.com',
    description='Some garbage server thing UI',
    requires=[
        'Tkinter',
        'CGIHTTPServer',
        'BaseHTTPServer',
        'urllib',
        'threading'
    ]
)
